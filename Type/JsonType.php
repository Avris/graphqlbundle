<?php

namespace Avris\GraphqlBundle\Type;

use GraphQL\Type\Definition\ScalarType;

class JsonType extends ScalarType
{
    public function serialize($value)
    {
        return json_encode($value);
    }

    public function parseValue($value)
    {
        return json_decode($value, true);
    }

    public function parseLiteral($valueNode, array $variables = null)
    {
        return json_decode($valueNode->value, true);
    }
}
