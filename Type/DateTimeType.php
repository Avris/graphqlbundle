<?php

namespace Avris\GraphqlBundle\Type;

use GraphQL\Type\Definition\ScalarType;

class DateTimeType extends ScalarType
{
    /**
     * @param \DateTimeInterface $value
     * @return int
     */
    public function serialize($value)
    {
        return $value->getTimestamp();
    }

    /**
     * @param \DateTimeInterface $value
     * @return int
     */
    public function parseValue($value)
    {
        return new \DateTimeImmutable('@' . $value);
    }

    /**
     * @param int $valueNode
     * @return \DateTimeInterface
     */
    public function parseLiteral($valueNode, array $variables = null)
    {
        return new \DateTimeImmutable('@' . $valueNode->value);
    }
}
