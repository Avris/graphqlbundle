<?php

namespace Avris\GraphqlBundle\Controller;

use Avris\GraphqlBundle\Service\GraphExecutor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GraphqlController
{
    public function main(GraphExecutor $graphExecutor, Request $request): Response
    {
        return $graphExecutor->execute($request);
    }
}
