<?php

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

function dqd($var, ...$moreVars)
{
    http_response_code(500);

    $cloner = new VarCloner();
    $dumper = new CliDumper();

    echo $dumper->dump($cloner->cloneVar($var), true);

    foreach ($moreVars as $var) {
        echo $dumper->dump($cloner->cloneVar($var), true);
    }

    die;
}
