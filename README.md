# Avris GraphqlBundle

Simplify Graphql configuration using plain methods with typehints and annotations.

This bundle is a wrapper on `webonyx/graphql-php`.

## Disclaimer

I'm new to GraphQL, currently writing my first project using it.
I'm sure that many things are missing from the bundle and many assumptions I made are wrong,
so please be understanding, and if possible help out with a pull request.

## Instalation

    $ composer require avris/graphql-bundle

Then add a route:

    avris_graphql_main:
        path: '/graph'
        controller: Avris\GraphqlBundle\Controller\GraphqlController::main

## Example

Let's say you have the following type in your app:

    <?php
    
    namespace App\Entity;
    
    final class User
    {
        // properties, constructor, etc.

        private static $type = null;
    
        final public static function type(): ObjectType
        {
            return static::$type ?: static::$type = static::buildType();
        }
    
        private static function buildType(): ObjectType
        {
            return new ObjectType([
                'name' => 'User',
                'fields' => [
                    'id' => [
                        'type' => Type::id(),
                        'resolve' => function (User $user) {
                            return $user->getId();
                        },
                    ],
                    'email' => [
                        'type' => Type::string(),
                        'resolve' => function (User $user) {
                            return $user->getEmail();
                        },
                    ],
                ],
            ]);
        }
        
        public function getId(): string
        {
            return $this->id;
        }

        public function getEmail(): string
        {
            return $this->email;
        }
    }

And the following GraphQL schema:

    new Schema([
        'query' => new ObjectType([
            'name' => 'Query',
            'fields' => [
                'hello' => [
                    'type' => Type::string(),
                    'resolve' => function () {
                        return 'GraphQL API';
                    }
                ],
                'user' => [
                    'type' => User::type(),
                    'args' => [
                        'id' => ['type' => Type::nonNull(Type::id())],
                    ],
                    'resolve' => $this->accessControl->guard(function ($root, $args) {
                        return $this->repository->find($args['id']);
                    }, 'ROLE_ADMIN'),
                ],
                'currentUser' => [
                    'type' => User::type(),
                    'resolve' => function ($root, $args) {
                        $user = $this->tokenStorage->getToken()->getUser();
        
                        return $user instanceof User ? $user : null;
                    },
                ],
                'jwt' => [
                    'type' => Type::string(),
                    'args' => [
                        'login' => Type::nonNull(Type::string()),
                        'password' => Type::nonNull(Type::string()),
                    ],
                    'resolve' => function ($root, $args) {
                        $user = $this->repository->findOneBy(['email' => $args['login']]);
        
                        if (!$user || !$this->encoder->isPasswordValid($user, $args['password'])) {
                            return null;
                        }
        
                        return (string) $this->jwtManager->issue($user);
                    },
                ]
            ],
        ]),
        'mutation' => new ObjectType([
            'name' => 'Mutation',
            'fields' => [
                'userRegistered' => [
                    'type' => User::type(),
                    'args' => [
                        'email' => Type::nonNull(Type::string()),
                        'password' => Type::nonNull(Type::string()),
                    ],
                    'resolve' => function ($root, $args) {
                        return $this->eventDispatcher->dispatch(new UserRegisteredEvent($args['email'], $args['password']));
                    },
                ]
            ],
        ]),
    ]);

Using this bundle, you can rewrite the entity/type like this:

    <?php
    
    namespace App\Entity;
    
    use Avris\GraphqlBundle\Annotation as Graphql;
    
    /**
     * @Graphql\Type
     */
    final class User
    {
        // properties, constructor, etc.
    
        /**
         * @Graphql\Query
         */
        public function getId(): string
        {
            return $this->id;
        }
    
        /**
         * @Graphql\Query
         */
        public function getEmail(): string
        {
            return $this->email;
        }
    }

And your schema/controllers like this:

    <?php
    
    namespace App\Controller;
    
    use Avris\GraphqlBundle\Annotation as Graphql;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    
    final class HomeController extends AbstractController
    {
        /**
         * @Graphql\Query
         */
        public function hello(): string
        {
            return 'GraphQL API';
        }
    }
    

and:

    <?php
    
    namespace App\Controller;
    
    use Avris\GraphqlBundle\Annotation as Graphql;
    use App\Entity\User;
    use App\Events\UserRegistered;
    
    final class UserController extends BaseController
    {
        // dependecies...
    
        /**
         * @Graphql\Query
         * @Graphql\Security("ROLE_ADMIN")
         * @Graphql\ParamType("id", var="id")
         */
        public function user(string $id): ?User
        {
            return $this->repository->find($id);
        }
    
        /**
         * @Graphql\Query
         */
        public function currentUser(): ?User
        {
            $user = $this->tokenStorage->getToken()->getUser();
    
            return $user instanceof User ? $user : null;
        }
    
        /**
         * @Graphql\Query
         */
        public function jwt(string $login, string $password): ?string
        {
            $user = $this->repository->findOneBy(['email' => $login]);
    
            if (!$user || !$this->encoder->isPasswordValid($user, $password)) {
                return null;
            }
    
            return (string) $this->jwtManager->issue($user);
        }
    
        /**
         * @Graphql\Query("mutation")
         */
        public function userRegistered(string $email, string $password): User
        {
            return $this->eventDispatcher->dispatch(new UserRegisteredEvent($email, $password));
        }
    }

The bundle will parse all the classes in `src/Controller` and `src/Entity`
(that list can be configured in the config key `avris_graphql.load`)
looking for the `Graphql` annotations.

All parameters of a query-method, as well as its return type have to be typehinted.
Supported built-in types are `string`, `bool`, `int` and `float`.
You can also typehint `json` and any type you registered with `@Graphql\Type`.

Additionally, you can use `id`, list (`int[]`) and union (`int|float`),
but for that you need an `@Graph\ParamType` or `@Graph\ReturnType` annotation for that.

For debugging you might find the function `dqd` useful (it stands for "dump in query and die").
It dumps vars (with `symfony/var-dumper`), sets HTTP response code to 500 (required for some GraphQL clients) and dies.  

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
