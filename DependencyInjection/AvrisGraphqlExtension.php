<?php

namespace Avris\GraphqlBundle\DependencyInjection;

use Avris\GraphqlBundle\Service\AnnotationLoader;
use Avris\GraphqlBundle\Service\GraphHandler;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class AvrisGraphqlExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $container
            ->registerForAutoconfiguration(GraphHandler::class)
            ->addTag('graph.handler');

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $loader->load('services.yaml');

        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition(AnnotationLoader::class);
        $definition->replaceArgument('$directories', $config['load']);
    }
}
