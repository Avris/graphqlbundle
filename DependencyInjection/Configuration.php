<?php

namespace Avris\GraphqlBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = (new TreeBuilder('avris_graphql'));

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('load')
                    ->prototype('scalar')
                ->end()
                ->defaultValue([
                    '%kernel.project_dir%/src/Entity',
                    '%kernel.project_dir%/src/Controller',
                ])
            ->end()
        ;

        return $treeBuilder;
    }
}
