<?php

namespace Avris\GraphqlBundle\Exception;

use GraphQL\Error\ClientAware;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AccessDenied extends AccessDeniedHttpException implements ClientAware
{
    public function isClientSafe()
    {
        return true;
    }

    public function getCategory()
    {
        return 'access';
    }
}
