<?php

namespace Avris\GraphqlBundle\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
final class Security
{
    /** @var string */
    private $check;

    /** @var bool */
    private $throw;

    public function __construct($values)
    {
        $this->check = $values['value'] ?? 'IS_AUTHENTICATED_REMEMBERED';
        $this->throw = $values['throw'] ?? true;
    }

    public function getCheck(): string
    {
        return $this->check;
    }

    public function shouldThrow(): bool
    {
        return $this->throw;
    }
}
