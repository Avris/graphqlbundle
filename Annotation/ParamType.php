<?php

namespace Avris\GraphqlBundle\Annotation;

use Avris\GraphqlBundle\Service\ReflectionType;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
final class ParamType
{
    /** @var string */
    private $var;

    /** @var string */
    private $type;

    public function __construct($values)
    {
        $this->var = $values['var'];
        $this->type = $values['value'];
    }

    public function getVar(): string
    {
        return $this->var;
    }

    public function getType(): ReflectionType
    {
        return new ReflectionType($this->type);
    }
}
