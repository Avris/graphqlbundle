<?php

namespace Avris\GraphqlBundle\Annotation;

use Avris\GraphqlBundle\Service\ReflectionType;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
final class ReturnType
{
    /** @var string */
    private $type;

    public function __construct($values)
    {
        $this->type = $values['value'];
    }

    public function getType(): ReflectionType
    {
        return new ReflectionType($this->type);
    }
}
