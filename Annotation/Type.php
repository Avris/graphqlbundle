<?php

namespace Avris\GraphqlBundle\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
final class Type
{
}
