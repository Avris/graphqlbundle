<?php

namespace Avris\GraphqlBundle\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
final class Query
{
    /** @var string */
    private $type;

    /** @var string */
    private $name;

    public function __construct($values)
    {
        $this->type = $values['value'] ?? 'query';
        $this->name = $values['name'] ?? null;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(\ReflectionMethod $method): string
    {
        return $this->name ?? preg_replace_callback('#^(?:get|is|has)([A-Z])#', function ($matches) {
                return strtolower($matches[1]);
            }, $method->getName());
    }
}
