<?php

namespace Avris\GraphqlBundle\Service;

use Avris\GraphqlBundle\Type\JsonType;
use Avris\GraphqlBundle\Type\DateTimeType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;

final class TypeManager
{
    /** @var ObjectType[] */
    private $types = [];

    public function __construct()
    {
        $this->types['json'] = new JsonType();
        $this->types['DateTimeInterface'] = new DateTimeType();
    }

    public function getType(\ReflectionType $type): Type
    {
        if ($type === null) {
            throw new \InvalidArgumentException('All parameters and return types need to be specified');
        }

        $gqlType = $this->getGqlType($type);

        return $type->allowsNull()
            ? $gqlType
            : Type::nonNull($gqlType);
    }

    private function getGqlType(\ReflectionType $type): Type
    {
        $name = $type->getName();

        switch ($name) {
            case 'id':
                return Type::id();
            case 'string':
                return Type::string();
            case 'bool':
                return Type::boolean();
            case 'int':
                return Type::int();
            case 'float':
                return Type::float();
            default:
                $parts = explode('|', $name);
                if (count($parts) > 1) {
                    return new UnionType([
                        'name' => $name,
                        'types' => array_map(function (string $typeName) {
                            return $this->getType(new ReflectionType($typeName));
                        }, $parts),
                    ]);
                }

                if (substr($name, -2) === '[]') {
                    return Type::listOf($this->getType(new ReflectionType(substr($name, 0, -2))));
                }

                try {
                    if (!isset($this->types[$name])) {
                        $this->register(new \ReflectionClass($name), []);
                    }

                    return $this->types[$name];
                } catch (\ReflectionException $e) {
                    throw new \InvalidArgumentException(sprintf('Unrecognised type "%s"', $type));
                }
        }
    }

    public function register(\ReflectionClass $class, array $fields)
    {
        if (!isset($this->types[$class->getName()])) {
            $this->types[$class->getName()] = new ObjectType([
                'name' => $class->getShortName(),
                'fields' => $fields,
            ]);

            return;
        }

        $this->types[$class->getName()]->config['fields'] += $fields;
    }
}
