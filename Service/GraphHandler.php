<?php

namespace Avris\GraphqlBundle\Service;

interface GraphHandler
{
    public function getQueries(): array;
}
