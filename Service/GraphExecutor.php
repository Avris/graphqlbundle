<?php

namespace Avris\GraphqlBundle\Service;

use GraphQL\Error\ClientAware;
use GraphQL\Error\Debug;
use GraphQL\Error\FormattedError;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Schema;
use GraphQL\GraphQL;

final class GraphExecutor
{
    /** @var int */
    private $debug = 0;

    /** @var GraphHandler[] */
    private $handlers;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(bool $debug, $handlers, LoggerInterface $logger)
    {
        if ($debug) {
            $this->debug = Debug::INCLUDE_DEBUG_MESSAGE | Debug::INCLUDE_TRACE;
        }
        $this->handlers = $handlers;
        $this->logger = $logger;
    }

    public function execute(Request $request): JsonResponse
    {
        try {
            $queries = [];
            foreach ($this->handlers as $handler) {
                $queries = array_merge_recursive($queries, $handler->getQueries());
            }

            $schema = [];
            foreach ($queries as $type => $fields) {
                $schema[$type] = new ObjectType([
                    'name' => ucfirst($type),
                    'fields' => $fields,
                ]);
            }

            $schema = new Schema($schema);

            $data = json_decode($request->getContent() ?: '', true);

            return new JsonResponse(
                GraphQL::executeQuery(
                    $schema,
                    $data['query'] ?? '{hello}',
                    null,
                    null,
                    $data['variables'] ?? []
                )->toArray($this->debug | Debug::RETHROW_INTERNAL_EXCEPTIONS),
                200
            );
        } catch (\Throwable $e) {
            if (!$e instanceof ClientAware || !$e->isClientSafe()) {
                $this->logger->critical(
                    get_class($e) . ': ' . $e->getMessage(),
                    ['exception' => $e]
                );
            }

            return new JsonResponse([
                'errors' => [ FormattedError::createFromException($e, $this->debug) ],
            ], 200);
        }
    }
}
