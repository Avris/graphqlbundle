<?php

namespace Avris\GraphqlBundle\Service;

use Avris\GraphqlBundle\Annotation\ParamType;
use Avris\GraphqlBundle\Annotation\Query;
use Avris\GraphqlBundle\Annotation\ReturnType;
use Avris\GraphqlBundle\Annotation\Security;
use Avris\GraphqlBundle\Annotation\Type;
use Avris\GraphqlBundle\Exception\AccessDenied;
use Psr\Container\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class AnnotationLoader implements GraphHandler
{
    /** @var string[] */
    private $directories = [];

    /** @var AnnotationReader */
    private $annotationReader;

    /** @var TypeManager */
    private $typeManager;

    /** @var \ReflectionClass[] [filename => class] */
    private $classes = [];

    /** @var ContainerInterface */
    private $container;

    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    public function __construct(
        array $directories,
        AnnotationReader $annotationReader,
        TypeManager $typeManager,
        ContainerInterface $container,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->directories = $directories;
        $this->annotationReader = $annotationReader;
        $this->typeManager = $typeManager;
        $this->container = $container;
        $this->authorizationChecker = $authorizationChecker;
        // TODO cache
    }

    public function getQueries(): array
    {
        $queries = [];

        $files = [];

        foreach ($this->directories as $directory) {
            if (!is_dir($directory)) {
                continue;
            }

            $finder = (new Finder())
                ->files()
                ->in($directory)
                ->name('*.php')
                ->ignoreDotFiles(true)
                ->ignoreUnreadableDirs(true)
                ->ignoreVCS(true);

            /** @var SplFileInfo $file */
            foreach ($finder as $file) {
                $files[] = $this->preloadFile($file);
            }
        }

        foreach ($files as $file) {
            foreach ($this->loadFile($file) as list($type, $name, $query)) {
                $queries[$type][$name] = $query;
            }
        }

        return $queries;
    }

    /**
     * Load the annotations first, because once class is used, AnnotationReader won't work
     */
    private function preloadFile(SplFileInfo $file)
    {
        $class = $this->getClass($file->getRealPath());

        $this->annotationReader->getClassAnnotations($class);

        return $file;
    }

    private function loadFile(SplFileInfo $file)
    {
        $class = $this->getClass($file->getRealPath());

        foreach ($this->annotationReader->getClassAnnotations($class) as $annotation) {
            if ($annotation instanceof Type) {
                $fields = [];
                foreach ($this->loadQueries($class) as list(, $name, $query)) {
                    $fields[$name] = $query;
                }
                $this->typeManager->register($class, $fields);
                return;
            }
        }

        yield from $this->loadQueries($class);
    }

    private function loadQueries(\ReflectionClass $class)
    {
        foreach ($class->getMethods() as $method) {
            $query = null;
            $security = null;
            $paramTypes = [];
            $returnType = null;

            foreach ($this->annotationReader->getMethodAnnotations($method) as $annotation) {
                if ($annotation instanceof Query) {
                    $query = $annotation;
                } elseif ($annotation instanceof Security) {
                    $security = $annotation;
                } elseif ($annotation instanceof ParamType) {
                    $paramTypes[$annotation->getVar()] = $annotation->getType();
                } elseif ($annotation instanceof ReturnType) {
                    $returnType = $annotation->getType();
                }
            }

            if ($query) {
                yield [
                    $query->getType(),
                    $query->getName($method),
                    $this->buildQuery($method, $security, $paramTypes, $returnType),
                ];
            }
        }
    }

    private function getClass(string $filename): \ReflectionClass
    {
        if (!isset($this->classes[$filename])) {
            require_once $filename;

            foreach (get_declared_classes() as $class) {
                $reflection = new \ReflectionClass($class);
                $filename = $reflection->getFileName();
                if ($filename !== false) {
                    $this->classes[$filename] = $reflection;
                }
            }
        }

        return $this->classes[$filename];
    }

    private function buildQuery(
        \ReflectionMethod $method,
        ?Security $security,
        array $paramTypes,
        ?\ReflectionType $returnType
    ) {
        return [
            'type' => $this->typeManager->getType($returnType ?: $method->getReturnType()),
            'args' => iterator_to_array($this->buildArguments($method, $paramTypes)),
            'resolve' => function ($root, $args) use ($method, $security) {
                $object = $root ?: $this->container->get($method->getDeclaringClass()->getName());

                if ($security && !$this->authorizationChecker->isGranted($security->getCheck(), $object)) {
                    if ($security->shouldThrow()) {
                        throw new AccessDenied();
                    }

                    return null;
                }

                return call_user_func_array(
                    [$object, $method->getName()],
                    $args
                );
            },
        ];
    }

    private function buildArguments(\ReflectionMethod $method, array $paramTypes)
    {
        foreach ($method->getParameters() as $parameter) {
            yield $parameter->getName()
                => $this->typeManager->getType($paramTypes[$parameter->getName()] ?? $parameter->getType());
        }
    }
}
