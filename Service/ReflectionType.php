<?php

namespace Avris\GraphqlBundle\Service;

final class ReflectionType extends \ReflectionType
{
    /** @var bool */
    private $nullable;

    /** @var string */
    private $typeName;

    public function __construct(string $name)
    {
        if (substr($name, 0, 1) === '?') {
            $this->nullable = true;
            $this->typeName = substr($name, 1);
        } else {
            $this->nullable = false;
            $this->typeName = $name;
        }
    }

    public function allowsNull()
    {
        return $this->nullable;
    }

    public function isBuiltin()
    {
        return false;
    }

    public function getName()
    {
        return $this->typeName;
    }

    public function __toString()
    {
        return $this->typeName;
    }
}
